require('dotenv').config();
const express = require('express');
const cookieParser = require('cookie-parser');
const { error } = require('./util/apiResponse');
const userRoutes = require('./routes/userRoutes');
const postsRoutes = require('./routes/postsRoutes');
const port = process.env.PORT || 3001;
const cors = require('cors');
const app = express();

// Começar a processar o corpo dos requests
app.use(express.json());
app.use(
  cors({
    origin: ['http://localhost:3000', 'http://localhost:8080', 'prodEnv'],
    credentials: true,
  }),
);
app.use(cookieParser());
app.use(express.static('uploads'));

app.get('/', (req, res) => {
  res.send('Hello World!');
});
app.use('/api/users', userRoutes);
app.use('/api/posts', postsRoutes);

app.use((err, req, res, next) => {
  console.error(err.stack);
  res
    .status(err.statusCode || 500)
    .send(error(err.message, err.statusCode || 500, err.errors));
});

app.listen(port, () => console.log('server running on port: ', port));
