require('dotenv').config();
const { sign, verify } = require('jsonwebtoken');
const { UnauthorizedError } = require('../util/errors');

const JWT_SECRECT = process.env.JWT_SECRECT;

const createTokens = (user) => {
  const accessToken = sign({ id: user.id }, JWT_SECRECT);
  return accessToken;
};

const validateToken = (req, res, next) => {
  try {
    const accessToken = req.cookies['ArtAveiro-login-token'];
    if (!accessToken) throw new UnauthorizedError();

    const validToken = verify(accessToken, JWT_SECRECT);

    //se o token for válido, continua com o pedido inserindo a propriedade authenticaded e userId com o username do utilizador logado
    if (validToken) {
      req.authenticated = true;
      req.userid = validToken.id;
      return next();
    } else {
      //se não for válido, erro  (provavelmente não é preciso este else porque estamos dentro de um try catch)
      throw new UnauthorizedError();
    }
  } catch (e) {
    throw new UnauthorizedError();
  }
};

module.exports = { createTokens, validateToken };
