const neo4j = require('neo4j-driver');

const { DB_HOST, DB_USER, DB_PASSWORD, DB_NAME } = process.env;

const driver = neo4j.driver(DB_HOST, neo4j.auth.basic(DB_USER, DB_PASSWORD));
let _session;

function _getSession() {
  if (!_session) {
    _session = driver.session({
      database: DB_NAME,
    });
  }
  return _session;
}

function query(query, params) {
  return _getSession().run(query, params);
}

module.exports.query = query;
