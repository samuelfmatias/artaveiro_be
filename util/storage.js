const { uuid } = require('uuidv4');
const path = require('path');
const mime = require('mime-types');

const STATIC_FOLDER = 'uploads';

// /uploads/prefix
function getUploadDir(prefix) {
  return path.join(STATIC_FOLDER, prefix);
}

// /Users/.../uploads/prefix
function getLocalUploadDir(prefix) {
  console.log(1);
  return path.join(path.dirname(require.main.filename), getUploadDir(prefix));
}

function generateRandomUniqueName(file) {
  console.log(2);
  const {
    // nome_original_do_ficheiro
    fieldname,
    mimetype,
  } = file;

  const uniqueId = uuid();
  const ext = mime.extension(mimetype);
  console.log(`${uniqueId}-${fieldname}.${ext}`);
  return `${uniqueId}-${fieldname}.${ext}`;
}

function returnFileType(file) {
  const {
    // nome_original_do_ficheiro
    mimetype,
  } = file;

  const ext = mime.extension(mimetype);

  return ext;
}

const whitelistFilesExt = ['image/png', 'image/jpeg', 'image/jpg'];

function isValidFileType(file) {
  return whitelistFilesExt.includes(file.mimetype);
}

module.exports = {
  STATIC_FOLDER,
  getLocalUploadDir,
  getUploadDir,
  generateRandomUniqueName,
  isValidFileType,
  returnFileType,
};
