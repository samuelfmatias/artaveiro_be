const express = require('express');
const Posts = require('../models/posts');
const { validateToken } = require('../util/JWT');
const router = express.Router();

//UPLOAD FILES
const { uploadFile } = require('../middleware/uploadFile');
const { returnFileType } = require('../util/storage');
// Criacao de um post e relacoes (post -> local / post -> person)
router.post(
  '/createPost',
  uploadFile.single('postContent'),
  async (req, res) => {
    const { placeId, personId, description } = req.body;

    const { filename: contentFilename, size: contentSize } = req.file;

    const avatar_image_url = `/postsContent/${filename}`;
    const contentType = returnFileType(req.file);
    Posts.createPost(placeId, personId, description, content, contentType)
      .then((data) => {
        res.status(200).json(data);
      })
      .catch((err) =>
        res.status(500).json({
          err: err,
        }),
      );
  },
);

// Like
router.post('/:id/createLike', (req, res) => {
  const { personId } = req.body;
  const { id: postId } = req.params;

  Posts.likePost(personId, postId)
    .then((data) => res.status(200).json({}))
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        err: 'error while fetching the data',
      });
    });
});

// comment
router.post('/:id/createComment', (req, res) => {
  const { personId } = req.body;
  const { id: postId } = req.params;
  const { comment } = req.params;

  Posts.commentPost(personId, postId, comment)
    .then((data) => res.status(200).json({}))
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        err: 'error while fetching the data',
      });
    });
});
router.get('/', validateToken, async (req, res) => {

  const result = await Posts.findAll();

  if (result.length === 0) {
    throw new NotFoundError('Nenhum utilizador encontrado');
  }

  const count = result.length;
  res.json(success(result, { totalItems: count }));
});


module.exports = router;
