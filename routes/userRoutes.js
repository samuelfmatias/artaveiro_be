const express = require('express');
const User = require('../models/user');
const { success, error } = require('../util/apiResponse');
const {
  NotFoundError,
  ValidationError,
  UnauthorizedError,
} = require('../util/errors');
const { validationResult } = require('express-validator');
const router = express.Router();

//Encriptar a Password
const bcrypt = require('bcryptjs');

//JWT
const { createTokens } = require('../util/JWT');
const { validateToken } = require('../util/JWT');
const { verify } = require('jsonwebtoken');

//UPLOAD FILES
const { uploadFile } = require('../middleware/uploadFile');

//-----------------GET----------------

router.get('/logout', (req, res) => {
  res
    .cookie('ArtAveiro-login-token', '', {
      httpOnly: true,
      expires: new Date(0),
    })
    .send();
});

/* Verificar se está logado */
router.get('/loggedIn', (req, res) => {
  try {
    const accessToken = req.cookies['ArtAveiro-login-token'];

    if (!accessToken) return res.send(false);

    verify(accessToken, process.env.JWT_SECRECT);
    res.send(true);
  } catch (e) {
    return res.send(false);
  }
});

//---------------POST-----------------

// publicar um novo user
router.post(
  '/signUp',
  uploadFile.single('file'),

  async (req, res) => {
    const {
      firstName,
      lastName,
      email,
      password,
      username,
      summary,
      artist_type,
    } = req.body;
    const { filename: avatarFilename, size: avatarSize } = req.file;
    //verificar se já existe um user com o mesmo email ou username
    const userExistsUsername = await User.findByUsername(username);
    const userExistsEmail = await User.findByEmail(email);

    const avatar_image_url = `/profileImages/${avatarFilename}`;

    if (userExistsUsername.length === 0 && userExistsEmail.length === 0) {
      //se não existir nenhum user registado com o username ou email inseridos, faz a query de insersão na BD

      bcrypt.hash(password, 10).then(async (hash) => {
        //Mandar o INSERT para a BD

        const user = await User.createUser(
          firstName,
          lastName,
          email,
          hash,
          username,
          summary,
          artist_type,
          avatar_image_url,
        )
          .then((data) => {
            //Se estiver correta, efetuar o login

            const accessToken = createTokens({ id: data.username });

            /* maxAge = 365 dias */
            res.cookie('ArtAveiro-login-token', accessToken, {
              maxAge: 60 * 60 * 24 * 365 * 1000,
              httpOnly: true,
            });

            res.json(success(data, {}, 'Utilizador criado com sucesso'));
          })
          .catch((err) =>
            //Se der erro, devolver o mesmo
            res.json(error('Erro na criação de utilizador', 500, err)),
          );
      });
    } else {
      throw ValidationError('O utilizador já existe');
    }
  },
);

router.post('/login', async (req, res) => {
  const { email, username, password } = req.body;

  await User.login(email, username).then((data) => {
    if (data.length === 0) {
      //Se não existir nenhum utilizador com essa credenciais
      throw UnauthorizedError('Credenciais Inválidas', 'Credenciais Inválidas');
    }
    //Se houver, comparar a password
    bcrypt.compare(password, data[0].password).then((match) => {
      //Se a password estiver errada
      if (!match) {
        //Devolver erro
        throw UnauthorizedError(
          'Credenciais Indálidas',
          'Credenciais Indálidas',
        );
      } else {
        //Se estiver correta, efetuar o login
        const accessToken = createTokens({ id: data[0].username });

        /* maxAge = 365 dias */
        res.cookie('ArtAveiro-login-token', accessToken, {
          maxAge: 60 * 60 * 24 * 365 * 1000,
          httpOnly: true,
        });

        res.json(success(data, {}, 'Utilizador logado com sucesso'));
      }
    });
  });
});

router.post('/:id/follow', validateToken, (req, res) => {
  // url params
  const { id: followedId } = req.params;
  const { id: followerId } = req.body;

  User.startsFollowing(followerId, followedId)
    .then((data) => {
      res.status(200).json();
    })
    .catch((err) =>
      res.status(500).json({
        err: err,
      }),
    );
});
//--------------PUT----------------
router.put('/:id', (req, res) => {
  // url params
  const { id } = req.params;
  // informação do body
  const { firstName, lastName, email, summary, username, verified, image_url } =
    req.body;

  User.update(
    id,
    firstName,
    lastName,
    email,
    summary,
    username,
    verified,
    image_url,
  )
    .then((data) => {
      res.status(200).json(data);
    })
    .catch((err) =>
      res.status(500).json({
        err: err,
      }),
    );
});

//------------------DELETE---------------
router.delete('/:id', (req, res) => {
  // url params
  const { id } = req.params;
  // TODO
});

router.delete('/:id/unfollow', (req, res) => {
  // url params
  const { id: followedId } = req.params;
  const { follower: followerId } = req.body;

  User.stopsFollowing(followerId, followedId)
    .then((data) => {
      res.status(200).json();
    })
    .catch((err) =>
      res.status(500).json({
        err: err,
      }),
    );
});

// ir buscar um user com um determinado email ou username
router.get('/:id', async (req, res) => {
  const userExistsUsername = await User.findByUsername(req.params.id);
  const userExistsEmail = await User.findByEmail(req.params.id);

  if (userExistsUsername.length === 0 && userExistsEmail.length === 0) {
    throw NotFoundError('User not found');
  }
  return res.json(success(userExistsUsername[0] || userExistsEmail[0]));
});

router.get('/', validateToken, async (req, res) => {
  console.log('bate aqui');
  const result = await User.findAll();

  if (result.length === 0) {
    throw new NotFoundError('Nenhum utilizador encontrado');
  }

  const count = result.length;
  res.json(success(result, { totalItems: count }));
});

module.exports = router;
