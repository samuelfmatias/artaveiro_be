const express = require('express');
const Filter = require('../models/filter');

const router = express.Router();

// filtar por texto
router.get('/filter', (req, res) => {
    Filter.filterAttachmentByText()
        .then((data) => res.json(data))
        .catch((err) => {
            console.log(err);
            res.status(500).json({
                err: 'error while fetching the data',
            });
        });
});


// filtar por imagem
router.get('/filter', (req, res) => {
    Filter.filterAttachmentByImage()
        .then((data) => res.json(data))
        .catch((err) => {
            console.log(err);
            res.status(500).json({
                err: 'error while fetching the data',
            });
        });
});

// filtar por Audio
router.get('/filter', (req, res) => {
    Filter.filterAttachmentByAudio()
        .then((data) => res.json(data))
        .catch((err) => {
            console.log(err);
            res.status(500).json({
                err: 'error while fetching the data',
            });
        });
});

// filtar por post de locais
router.get('/filter', (req, res) => {
    const { placeName } = req.params;
    Filter.filterByPlace(placeName)
        .then((data) => res.json(data))
        .catch((err) => {
            console.log(err);
            res.status(500).json({
                err: 'error while fetching the data',
            });
        });
});

// filtar por post de um user
router.get('/filter', (req, res) => {
    const { id: userId } = req.body;
    Filter.filterByPlace(userId)
        .then((data) => res.json(data))
        .catch((err) => {
            console.log(err);
            res.status(500).json({
                err: 'error while fetching the data',
            });
        });
});



module.exports = router;