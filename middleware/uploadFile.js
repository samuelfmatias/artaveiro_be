const multer = require('multer');

const {
  getUploadDir,
  getLocalUploadDir,
  generateRandomUniqueName,
  isValidFileType,
} = require('../util/storage');

const storage = multer.diskStorage({
  // destino onde os ficheiros devem ser guardados
  destination: (req, file, cb) => {
    console.log(0000);
    cb(null, getLocalUploadDir('profileImages'));
  },
  // nome do ficheiro
  filename: (req, file, cb) => {
    console.log(1111);
    cb(null, generateRandomUniqueName(file));
  },
});

const uploadFile = multer({
  storage,
  fileFilter: (req, file, cb) => {
    console.log('PUTAAAAAAAAAAAAAAAAAAAAAAAAA', file);
    // 1º validação se o ficheiro é de um tipo válido
    if (!isValidFileType(file)) {
      return cb(new BadRequest('File type is not allowed'));
    }
    return cb(null, true);
  },
  // validação por ficheiro se passa do limite máximo
  limits: { fileSize: 1048576 }, // 10mb per file
});

module.exports = {
  uploadFile,
};
