const { int } = require('neo4j-driver');
const { query } = require('../util/database');

//filtar posts por apenas texto
function filterAttachmentByText() {
    return query(
        `MATCH (n:POST)-[r]-(a:ATTACH{type:"1"}) RETURN n,r,a `,
    );
}


//filtar posts por apenas imagem
function filterAttachmentByImage() {
    return query(
        `MATCH (n:POST)-[r]-(a:ATTACH{type:"2"}) RETURN n,r,a `,
    );
}


//filtar posts por apenas Audio
function filterAttachmentByAudio() {
    return query(
        `MATCH (n:POST)-[r]-(a:ATTACH{type:"3"}) RETURN n,r,a `,
    );
}

//filtar posts por local
function filterByPlace(placeName) {
    return query(
        `MATCH (n:POST)-[r]-(p:PLACE{name:$placeName}) RETURN n,r,p`,
        {
            placeName,
        },
    );
}


//n de publicacoes de um user

function filterByUserPosts(id) {
    return query(
        `MATCH (n:POST)-[r]-(p:PERSON{id:$id}) RETURN n,r,p`,
        {
            id,
        },
    );
}


//n de seguidores

//n de A seguir




module.exports = {
    filterAttachmentByText,
    filterAttachmentByImage,
    filterAttachmentByAudio,
    filterByPlace,
    filterByUserPosts,

};
