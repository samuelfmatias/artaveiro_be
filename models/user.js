const { int } = require('neo4j-driver');
const { query } = require('../util/database');

function findAll() {
  return query(
    'MATCH (personNode:PERSON) RETURN personNode, ID(personNode) AS id',
  ).then((result) => {
    return result.records.map((record) => ({
      id: record.get('id').toString(),
      ...record.get('personNode').properties,
    }));
  });
}

function login(email = '', username = '') {
  return query(
    'MATCH (p:PERSON) WHERE p.username = $username OR p.email = $email RETURN p ',
    {
      username,
      email,
    },
  )
    .then((result) => {
      return result.records.map((record) => record.get('p').properties);
    })
    .catch((err) => console.log('err', err));
}

function findByUsername(username) {
  return query('MATCH (p:PERSON) WHERE p.username = $username RETURN p', {
    username,
  })
    .then((result) => {
      return result.records.map((record) => record.get('p').properties);
    })
    .catch((err) => console.log('err', err));
}

function findByEmail(email) {
  return query('MATCH (p:PERSON) WHERE p.email = $email RETURN p', {
    email,
  })
    .then((result) => {
      return result.records.map((record) => record.get('p').properties);
    })
    .catch((err) => console.log('err', err));
}

function createUser(
  firstName,
  lastName,
  email,
  password,
  username,
  summary,
  artist_type,
  avatar_image_url,
  cover_image_url,
) {
  return query(
    `CREATE (n:PERSON {firstName: $firstName, lastName: $lastName, email: $email, password: $password, artist_type: $artist_type , summary: $summary, username: $username, avatar_image_url: $avatar_image_url, createdAt: datetime()})  RETURN n`,
    {
      firstName,
      lastName,
      email,
      password,
      username,
      summary,
      artist_type,
      avatar_image_url,
    },
  ).then((result) => {
    return result.records[0].get('n').properties;
  });
}

/* function createArtist(
  firstName,
  lastName,
  email,
  summary,
  username,
  verified = false,
  image_url,
  artistType,
) {
  return query(
    `CREATE (n:PERSON:ARTIST {id: randomUUID(), firstName: $firstName, lastName: $lastName, email: $email, summary: $summary, username: $username, verified: $verified, image_url: $image_url, createdAt: datetime(), artistType: $artistType}) RETURN n`,
    {
      firstName,
      lastName,
      email,
      summary,
      username,
      verified,
      image_url,
      artistType,
    },
  );
} */

function update(
  id,
  firstName,
  lastName,
  email,
  summary,
  username,
  verified = false,
  image_url,
) {
  return query(
    `MATCH (n:PERSON) WHERE ID(n) = $id SET n.email = $email, n.firstName = $firstName, n.lastName = $lastName, n.summary = $summary, n.username = $username, n.verified = $verified, n.image_url = $image_url RETURN n`,
    {
      id,
      firstName,
      lastName,
      email,
      summary,
      username,
      verified,
      image_url,
    },
  ).then((result) => result.records[0].get('n').properties);
}

function remove() {
  // TODO: remove query
}

function startsFollowing(followerId, followedId) {
  return query(
    `MATCH (follower:PERSON) WHERE ID(follower) = $followerId MATCH (followed:PERSON) WHERE ID(followed) = $followedId CREATE (follower)-[f:FOLLOWS]->(followed)`,
    {
      followerId,
      followedId,
    },
  );
}

function stopsFollowing(followerId, followedId) {
  return query(
    `MATCH (follower:PERSON)-[f:FOLLOWS]->(followed:PERSON) WHERE ID(follower) = 0 AND ID(followed) = 1 DELETE f`,
    {
      followerId,
      followedId,
    },
  );
}

module.exports = {
  findAll,
  findByUsername,
  findByEmail,
  createUser,
  login,
  // createArtist,
  update,
  remove,
  startsFollowing,
  stopsFollowing,
};
