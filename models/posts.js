const { int } = require('neo4j-driver');
const { query } = require('../util/database');

function createPost(placeId, personId, description, content, contentType) {
  personId = parseInt(personId);
  placeId = parseInt(placeId);
  return query(
    ` MATCH (p:PLACE) WHERE ID(p) = $placeId MATCH (person:PERSON) WHERE ID(person) = $personId
        CREATE (person)-[a:PUBLISHED]->(n:POST {description:$description, createdAt: datetime()})-[r:LOCATED_AT]->(p)
        ${content &&
    'CREATE (n)-[c:HAS]->(i:ATTACHMENT{content:$content, contentType:$contentType, createdAt: datetime()})'
    }
       
        RETURN p,person, n,r,i,c`,

    {
      placeId,
      personId,
      description,
      content,
      contentType,
    },
  ).then((result) => {
    return {
      object: result.records[0].get('n').properties,
      actor: result.records[0].get('person').properties,
      attachment: result.records[0].get('i').properties || null,
      place: result.records[0].get('p').properties,
    };
  });
}

function likePost(personId, postId) {
  return query(
    `MATCH (p:PERSON) WHERE ID(p) = $personId MATCH(n:POST) WHERE ID(n) = $postId CREATE (p)-[r:LIKES]->(n)`,
    {
      personId,
      postId,
    },
  );
}

function commentPost(personId, postId, comment) {
  return query(
    `MATCH (p:PERSON)  WHERE ID(p) = $personId MATCH(n:POST )  WHERE ID(n) = $postId CREATE (p)-[r:COMMENT {content:$comment, createdAt: datetime() }]->(n)`,
    {
      personId,
      postId,
      comment,
    },
  );
}

function findAll() {
  return query(
    'MATCH (n:POST) RETURN n',
  ).then((result) => {
    return result.records.map((record) => ({
      id: record.get('n').toString(),
    }));
  });
}

module.exports = {
  createPost,
  likePost,
  commentPost, findAll
};
